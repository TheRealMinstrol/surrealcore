package com.surrealcraft.SoSurreal;

        import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class MyDeathListener
        implements Listener
{
    private Main plugin;

    public MyDeathListener(Main instance)
    {
        this.plugin = instance;
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event)
    {
        Player player = event.getPlayer();
        if ((this.plugin.getConfig().isSet("users." + player.getName() + ".home.world")) && (this.plugin.getConfig().isSet("users." + player.getName() + ".home.x")) && (this.plugin.getConfig().isSet("users." + player.getName() + ".home.y")) && (this.plugin.getConfig().isSet("users." + player.getName() + ".home.z")))
        {
            Location loc = new Location(this.plugin.getServer().getWorld(this.plugin.getConfig().getString("users." + player.getName() + ".home.world")), this.plugin.getConfig().getDouble("users." + player.getName() + ".home.x"), this.plugin.getConfig().getDouble("users." + player.getName() + ".home.y"), this.plugin.getConfig().getDouble("users." + player.getName() + ".home.z"));
            loc.setYaw(this.plugin.getConfig().getInt("users." + player.getName() + ".home.d"));
            event.setRespawnLocation(loc);
        }
    }
}
