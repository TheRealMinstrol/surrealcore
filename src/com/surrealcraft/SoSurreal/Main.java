package com.surrealcraft.SoSurreal;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin implements Listener {
    protected static Connection connection;
    private HashMap<String, Integer> tasks = new HashMap();
    private Map<String, String> messagers = new HashMap<String, String>();
    private ArrayList<String> pvping = new ArrayList();
    public ArrayList<Player> afker = new ArrayList();
    public String prefix = ChatColor.RED + "" + ChatColor.BOLD + "[Surreal]" + ChatColor.DARK_RED + "" + ChatColor.BOLD + " ";
    public String staffChatPrefix = ChatColor.YELLOW + "" + ChatColor.BOLD + "STAFF " + ChatColor.RED + "> " + ChatColor.RESET;
    public ArrayList<Player> nonstaff = new ArrayList();
    public ArrayList<Player> staff = new ArrayList();
    public ArrayList<String> mutedPlayers = new ArrayList();
    public ArrayList<String> flyingStaff = new ArrayList();
    public ArrayList<String> Vanished = new ArrayList();
    public ArrayList<String> cooldown = new ArrayList();
    public Location spawn = new Location(Bukkit.getWorld("world"), 310, 87, 240);
    Map<String, Long> tpaCooldown = new HashMap();
    Map<String, String> currentRequest = new HashMap();


    @Override
    public void onEnable() {
        for (Player p : Bukkit.getOnlinePlayers()){
            p.kickPlayer(ChatColor.YELLOW + "Server enabling.. " + ChatColor.GRAY + "(Please relog)");
        }
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }

    public void vanish(final Player staff) {
        tasks.put(staff.getName(), Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("SurrealCore"), new Runnable() {
            @Override
            public void run() {
                for (Player nonstaffPlayers : nonstaff) {
                    nonstaffPlayers.hidePlayer(staff);
                }
            }
        }, 0L, 20L));
    }

    public void unvanish(Player staff) {
        if (Vanished.contains(staff.getName())) {
            Bukkit.getServer().getScheduler().cancelTask(tasks.get(staff.getName()));
            Vanished.remove(staff.getName());
        }
        for (Player nonstaffPlayers : nonstaff) {
            nonstaffPlayers.showPlayer(staff);
        }

    }

    public void loadConfig()
    {
        getConfig().addDefault("tpa-cooldown", Integer.valueOf(5));
        getConfig().addDefault("keep-alive", Integer.valueOf(30));
        getConfig().options().copyDefaults(true);
        saveConfig();
    }
    public void sendRequest(Player sender, Player recipient)
    {
        sender.sendMessage("Sending a teleport request to " + recipient.getName() + ".");

        String sendtpaccept = "";
        String sendtpdeny = "";
        if (recipient.hasPermission("tpa.tpaccept")) {
            sendtpaccept = " To accept the teleport request, type " + ChatColor.GOLD + "/tpaccept" + ChatColor.RESET + ".";
        } else {
            sendtpaccept = "";
        }
        if (recipient.hasPermission("tpa.tpdeny")) {
            sendtpdeny = " To deny the teleport request, type " + ChatColor.GOLD + "/tpdeny" + ChatColor.RESET + ".";
        } else {
            sendtpdeny = "";
        }
        recipient.sendMessage(ChatColor.GOLD + sender.getName() + ChatColor.RESET + " has sent a request to teleport to you." + sendtpaccept + sendtpdeny);
        this.currentRequest.put(recipient.getName(), sender.getName());
    }
    public boolean killRequest(String key)
    {
        if (this.currentRequest.containsKey(key))
        {
            Player loser = getServer().getPlayer((String)this.currentRequest.get(key));
            if (loser != null) {
                loser.sendMessage(ChatColor.GOLD + "Your teleport request timed out.");
            }
            this.currentRequest.remove(key);

            return true;
        }
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        final Player player = (Player) sender;
        Player p = null;
        if (command.getName().equalsIgnoreCase("tpa"))
        {
            if (p != null)
            {
                if (!p.hasPermission("tpa.bypasscooldown"))
                {
                    int cooldown = getConfig().getInt("tpa-cooldown");
                    if (this.tpaCooldown.containsKey(p.getName()))
                    {
                        long diff = (System.currentTimeMillis() - ((Long)this.tpaCooldown.get(sender.getName())).longValue()) / 1000L;
                        if (diff < cooldown)
                        {
                            p.sendMessage(ChatColor.AQUA + "You must wait a " + cooldown + " second cooldown in between teleport requests!");
                            return false;
                        }
                    }
                }
                if (args.length > 0)
                {
                    final Player target = getServer().getPlayer(args[0]);
                    long keepAlive = getConfig().getLong("keep-alive") * 20L;
                    if (target == null)
                    {
                        sender.sendMessage(ChatColor.AQUA + "You can only send a teleport request to online players!");
                        return false;
                    }
                    if (target == p)
                    {
                        sender.sendMessage(ChatColor.AQUA + "You can't teleport to yourself!");
                        return false;
                    }
                    sendRequest(p, target);

                    getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
                    {
                        public void run()
                        {
                            killRequest(target.getName());
                        }
                    }, keepAlive);

                    this.tpaCooldown.put(p.getName(), Long.valueOf(System.currentTimeMillis()));
                }
                else
                {
                    p.sendMessage("Send a teleport request to a player.");
                    p.sendMessage("/tpa <player>");
                }
            }
            else
            {
                sender.sendMessage(ChatColor.RED + "The console isn't a player");
                return false;
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("tpaccept"))
        {
            if (p != null)
            {
                if (this.currentRequest.containsKey(p.getName()))
                {
                    Player heIsGoingOutOnADate = getServer().getPlayer((String)this.currentRequest.get(p.getName()));
                    this.currentRequest.remove(p.getName());
                    if (heIsGoingOutOnADate != null)
                    {
                        heIsGoingOutOnADate.teleport(p);
                        p.sendMessage(ChatColor.GRAY + "Teleporting...");
                        heIsGoingOutOnADate.sendMessage(ChatColor.GRAY + "Teleporting...");
                    }
                    else
                    {
                        sender.sendMessage(ChatColor.AQUA + "It seems the player to you were teleporting to has left the server!");
                        return false;
                    }
                }
                else
                {
                    sender.sendMessage(ChatColor.AQUA + "You doesn't have any current tp requests.");
                    return false;
                }
            }
            else
            {
                sender.sendMessage(ChatColor.AQUA + "The console isn't a player");
                return false;
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("tpdeny"))
        {
            if (p != null)
            {
                if (this.currentRequest.containsKey(p.getName()))
                {
                    Player poorRejectedGuy = getServer().getPlayer((String)this.currentRequest.get(p.getName()));
                    this.currentRequest.remove(p.getName());
                    if (poorRejectedGuy != null)
                    {
                        poorRejectedGuy.sendMessage(ChatColor.GOLD + p.getName() + " rejected your teleport request! :(");
                        p.sendMessage(ChatColor.GRAY + poorRejectedGuy.getName() + " was rejected!");
                        return true;
                    }
                }
                else
                {
                    sender.sendMessage(ChatColor.GOLD + "You doesn't have any current tp requests.");
                    return false;
                }
            }
            else
            {
                sender.sendMessage(ChatColor.GOLD + "The console isn't a player");
                return false;
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("pvp")) {
            if (pvping.contains(player.getName())){
                player.sendMessage(prefix + ChatColor.YELLOW + "Your pvp is now " + ChatColor.GREEN + "ON");
                pvping.remove(player.getName());
                openConnection();
                try {
                    boolean flying;
                    if (playerDataContainsPlayer(player.getPlayer())) {
                        PreparedStatement sql = connection.prepareStatement("SELECT flying FROM `surreal_data` WHERE player=?;");
                        sql.setString(1, player.getPlayer().getName());

                        ResultSet result = sql.executeQuery();
                        result.next();

                        flying = result.getBoolean("flying");

                        PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET flying=? WHERE player=?");
                        loginsupdate.setBoolean(1, false);
                        loginsupdate.setString(2, player.getPlayer().getName());
                        loginsupdate.executeUpdate();

                        loginsupdate.close();
                        sql.close();
                        result.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    closeConnection();
                }
            } else {
                player.sendMessage(prefix + ChatColor.YELLOW + "Your pvp is now " + ChatColor.RED + "OFF");
                pvping.add(player.getName());
                openConnection();
                try {
                    boolean pvping;
                    if (playerDataContainsPlayer(player.getPlayer())) {
                        PreparedStatement sql = connection.prepareStatement("SELECT pvping FROM `surreal_data` WHERE player=?;");
                        sql.setString(1, player.getPlayer().getName());

                        ResultSet result = sql.executeQuery();
                        result.next();

                        pvping = result.getBoolean("pvping");

                        PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET pvping=? WHERE player=?");
                        loginsupdate.setBoolean(1, true);
                        loginsupdate.setString(2, player.getPlayer().getName());
                        loginsupdate.executeUpdate();

                        loginsupdate.close();
                        sql.close();
                        result.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    closeConnection();
                }
            }
        }
        if (command.getName().equalsIgnoreCase("fly")) {
            if (player.hasPermission("surreal.fly")) {
                if (flyingStaff.contains(player.getName())) {
                    player.sendMessage(prefix + ChatColor.YELLOW + "You are no longer flying!");
                    player.setFlying(false);
                    player.setAllowFlight(false);
                    flyingStaff.remove(player.getName());
                    openConnection();
                    try {
                        boolean flying;
                        if (playerDataContainsPlayer(player.getPlayer())) {
                            PreparedStatement sql = connection.prepareStatement("SELECT flying FROM `surreal_data` WHERE player=?;");
                            sql.setString(1, player.getPlayer().getName());

                            ResultSet result = sql.executeQuery();
                            result.next();

                            flying = result.getBoolean("flying");

                            PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET flying=? WHERE player=?");
                            loginsupdate.setBoolean(1, false);
                            loginsupdate.setString(2, player.getPlayer().getName());
                            loginsupdate.executeUpdate();

                            loginsupdate.close();
                            sql.close();
                            result.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        closeConnection();
                    }
                    return true;
                } else {
                    player.setAllowFlight(true);
                    player.setFlying(true);
                    player.sendMessage(prefix + ChatColor.YELLOW + "You are now flying!");
                    flyingStaff.add(player.getName());
                    openConnection();
                    try {
                        boolean flying;
                        if (playerDataContainsPlayer(player.getPlayer())) {
                            PreparedStatement sql = connection.prepareStatement("SELECT flying FROM `surreal_data` WHERE player=?;");
                            sql.setString(1, player.getPlayer().getName());

                            ResultSet result = sql.executeQuery();
                            result.next();

                            flying = result.getBoolean("flying");

                            PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET flying=? WHERE player=?");
                            loginsupdate.setBoolean(1, true);
                            loginsupdate.setString(2, player.getPlayer().getName());
                            loginsupdate.executeUpdate();

                            loginsupdate.close();
                            sql.close();
                            result.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        closeConnection();
                    }
                    return true;
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("heal")) {
            if (player.hasPermission("surreal.heal")) {
                if (args.length == 0) {
                    player.setHealth(20);
                    player.sendMessage(prefix + ChatColor.GREEN + "You have been healed!");
                    player.getActivePotionEffects().clear();
                    return true;
                }
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if (target == null) {
                    player.sendMessage(prefix + "That player is currently offline");
                    return true;
                }
                target.getActivePotionEffects().clear();
                target.setHealth(20);
                target.sendMessage(prefix + ChatColor.GREEN + "You have been healed by " + ChatColor.YELLOW + player.getName());
                player.sendMessage(prefix + ChatColor.GREEN + "You have healed " + ChatColor.YELLOW + target.getName());
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("alert")) {
            if (player.hasPermission("surreal.alert")) {
                if (args.length == 0) {
                    player.sendMessage(prefix + ChatColor.GOLD + "/alert <message>");
                    return true;
                }
                String msg = "";
                String[] arrayOfString;
                int j = (arrayOfString = args).length;
                for (int i = 0; i < j; i++) {
                    String s = arrayOfString[i];
                    msg = msg + " " + s;
                }
                Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + "" + ChatColor.BOLD + "ALERT " + ChatColor.BOLD + "" + ChatColor.GRAY + ">" + ChatColor.YELLOW + "" + ChatColor.BOLD + msg));
                return true;
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("coins")) {
            int previousCoins = 0;
            Player target = Bukkit.getServer().getPlayer(args[3]);
            if (player.hasPermission("surreal.coins")){
                if (args.length == 0) {
                    player.sendMessage(prefix + ChatColor.YELLOW + "Usage: /coins <ADD,REMOVE,SET,AMOUNT> <PLAYER>");
                    return true;
                }
                if (args.length == 1){
                    if (args[1].equalsIgnoreCase("add")){
                        player.sendMessage(prefix + ChatColor.YELLOW + "Please specify the amount of coins you want to add!");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("remove")){
                        player.sendMessage(prefix + ChatColor.YELLOW + "Please specify the amount of coins you want to remove!");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("amount")){
                        player.sendMessage(prefix + ChatColor.YELLOW + "Please specify the player!");
                        return true;
                    }
                }
                int amount = 0;
                Player smalltarget = Bukkit.getServer().getPlayer(args[2]);
                if (args.length == 2){
                    if (args[1].equalsIgnoreCase("add")) {
                        if (args[2].equals(amount)){
                            player.sendMessage(prefix + ChatColor.YELLOW + "Please specify the player!");
                            return true;
                        }
                    }
                    if (args[1].equalsIgnoreCase("remove")) {
                        if (args[2].equals(amount)){
                            player.sendMessage(prefix + ChatColor.YELLOW + "Please specify the player!");
                            return true;
                        }
                    }
                    if (args[1].equalsIgnoreCase("amount")) {
                        if (smalltarget == null) {
                            player.sendMessage(prefix + ChatColor.YELLOW + "Player not found!");
                            return true;
                        } else {
                            openConnection();
                            try {
                                if (playerDataContainsPlayer(target.getPlayer())) {
                                    PreparedStatement statement = connection.prepareStatement("SELECT coins FROM `surreal_data` WHERE player=?;");
                                    statement.setString(1, target.getPlayer().getName());
                                    ResultSet resultSet = statement.executeQuery();
                                    resultSet.next();
                                    previousCoins = resultSet.getInt("coins");
                                    player.sendMessage(prefix + ChatColor.YELLOW + smalltarget.getName() + "'s coin amount: " + previousCoins);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                closeConnection();
                            }
                        }
                    }
                }
                if (args.length == 3){
                    if (args[1].equalsIgnoreCase("add")) {
                        if (args[2].equals(amount)){
                            if (args[3].equalsIgnoreCase(target.getName())) {
                                if (target == null){
                                    player.sendMessage(prefix + ChatColor.YELLOW + "Player not found!");
                                    return true;
                                } else {
                                    openConnection();
                                    try {
                                        if (playerDataContainsPlayer(target.getPlayer())) {
                                            PreparedStatement statement = connection.prepareStatement("SELECT coins FROM `surreal_data` WHERE player=?;");
                                            statement.setString(1, target.getPlayer().getName());
                                            ResultSet resultSet = statement.executeQuery();
                                            resultSet.next();

                                            previousCoins = resultSet.getInt("coins");

                                            PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `surreal_data` SET coins=? WHERE player=?");
                                            loginsUpdate.setInt(1, previousCoins + amount);
                                            loginsUpdate.setString(2, target.getPlayer().getName());
                                            loginsUpdate.executeUpdate();

                                            loginsUpdate.close();
                                            statement.close();
                                            resultSet.close();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        closeConnection();
                                    }
                                }
                            }
                        }
                    }
                    if (args[1].equalsIgnoreCase("remove")) {
                        if (args[2].equals(amount)){
                            if (args[3].equalsIgnoreCase(target.getName())) {
                                if (target == null){
                                    player.sendMessage(prefix + ChatColor.YELLOW + "Player not found!");
                                    return true;
                                } else {
                                    openConnection();
                                    try {
                                        if (playerDataContainsPlayer(target.getPlayer())) {
                                            PreparedStatement statement = connection.prepareStatement("SELECT coins FROM `surreal_data` WHERE player=?;");
                                            statement.setString(1, target.getPlayer().getName());
                                            ResultSet resultSet = statement.executeQuery();
                                            resultSet.next();

                                            previousCoins = resultSet.getInt("coins");

                                            PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `surreal_data` SET coins=? WHERE player=?");
                                            loginsUpdate.setInt(1, previousCoins - amount);
                                            loginsUpdate.setString(2, target.getPlayer().getName());
                                            loginsUpdate.executeUpdate();

                                            loginsUpdate.close();
                                            statement.close();
                                            resultSet.close();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        closeConnection();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("feed")) {
            if (player.hasPermission("surreal.feed")) {
                if (args.length == 0) {
                    player.setFoodLevel(20);
                    player.sendMessage(prefix + ChatColor.GREEN + "You have been fed!");
                    return true;
                }
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if (target == null) {
                    player.sendMessage(prefix + "That player is currently offline");
                    return true;
                }
                target.setFoodLevel(20);
                target.sendMessage(prefix + ChatColor.GREEN + "You have been fed by " + ChatColor.YELLOW + player.getName());
                player.sendMessage(prefix + ChatColor.GREEN + "You have fed " + ChatColor.YELLOW + target.getName());
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("gms")) {
            if (player.hasPermission("surreal.gm")) {
                player.setGameMode(GameMode.SURVIVAL);
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }

        }
        if (command.getName().equalsIgnoreCase("gmc")) {
            if (player.hasPermission("surreal.gm")) {
                player.setGameMode(GameMode.CREATIVE);
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("gma")) {
            if (player.hasPermission("surreal.gm")) {
                player.setGameMode(GameMode.ADVENTURE);
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("afk")) {
            if (player.hasPermission("surreal.afk")) {
                if (afker.contains(player)) {
                    PermissionUser sUser = PermissionsEx.getUser(player.getPlayer());
                    String sPrefix = sUser.getPrefix();
                    afker.remove(player);
                    Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "- " + sPrefix + "" + player.getName() + ChatColor.GOLD + " is no longer AFK!");
                    player.sendMessage(prefix + ChatColor.GOLD + "You are no longer afk, all private messages will unblocked!");
                } else {
                    afker.add(player);
                    player.sendMessage(prefix + ChatColor.GOLD + "You are now afk, all private messages will blocked!");
                    PermissionUser sUser = PermissionsEx.getUser(player.getPlayer());
                    String sPrefix = sUser.getPrefix();
                    Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "- " + sPrefix + "" + player.getName() + ChatColor.GOLD + " is now AFK!");
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("msg")) {
            if (args.length < 2) {
                player.sendMessage(prefix + ChatColor.GOLD + "/msg <player> <message>");
                return true;
            }
            if (afker.contains(player)) {
                player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently afk, please try again!");
                return true;
            }
            Player target = (Bukkit.getServer().getPlayer(args[0]));
            if (target == null) {
                player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently offline!");
                return true;
            }
            if (mutedPlayers.contains(player)) {
                player.sendMessage(prefix + ChatColor.GOLD + "You are Muted!");
                return true;
            } else {
                String message = "";
                for (int i = 1; i < args.length; i++)
                    message += args[i] + " ";
                PermissionUser sUser = PermissionsEx.getUser(player);
                PermissionUser targerUser = PermissionsEx.getUser(target);
                String sPrefix = sUser.getPrefix();
                String targetPrefix = targerUser.getPrefix();
                target.sendMessage(ChatColor.LIGHT_PURPLE + "From " + ChatColor.RESET + sPrefix + "" + player.getName() + " " + ChatColor.YELLOW + message);
                player.sendMessage(ChatColor.LIGHT_PURPLE + "To " + ChatColor.RESET + targetPrefix + "" + target.getName() + " " + ChatColor.YELLOW + message);
                messagers.put(player.getName(), target.getName());
            }
        }
        if (command.getName().equalsIgnoreCase("kick")) {
            if (player.hasPermission("surreal.kick")) {
                if (args.length < 2) {
                    player.sendMessage(prefix + ChatColor.GOLD + "/kick <player> <reason>");
                    return true;
                }
                Player target = (Bukkit.getServer().getPlayer(args[0]));
                if (target == null) {
                    player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently offline!");
                    return true;
                }
                if (target.hasPermission("surreal.cantkick")) {
                    player.sendMessage(prefix + ChatColor.YELLOW + "You can not kick this person!");
                    return true;
                }
                PermissionUser sUser = PermissionsEx.getUser(player);
                PermissionUser targerUser = PermissionsEx.getUser(target);
                if (sUser.inGroup("mod") && targerUser.inGroup("mod") || sUser.inGroup("mod") && targerUser.inGroup("admin") || sUser.inGroup("mod") && targerUser.inGroup("dev") || sUser.inGroup("admin") && targerUser.inGroup("dev")){
                    player.sendMessage(prefix + ChatColor.GOLD + "You can not kick this person!");
                    return true;
                } else {
                    String message = "";
                    for (int i = 1; i < args.length; i++)
                        message += args[i] + " ";
                    String sPrefix = sUser.getPrefix();
                    String targetPrefix = targerUser.getPrefix();
                    target.kickPlayer(ChatColor.GOLD + "You were kicked from the server! Reason: " + ChatColor.RED + message);
                    for (Player staffy : staff) {
                        staffy.sendMessage(staffChatPrefix + sPrefix + player.getName() + ChatColor.GOLD + " has kicked " + targetPrefix + "" + target.getName() + ChatColor.GOLD + " Reason: " + ChatColor.RED + message);
                    }
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("r")) {
            if (args.length == 0) {
                player.sendMessage(prefix + ChatColor.GOLD + "/r <message>");
                return true;
            }

            if (!messagers.containsKey(player.getName())) {
                player.sendMessage(prefix + ChatColor.GOLD + "You must message a player first!");
                return true;
            }

            Player target = Bukkit.getPlayer(messagers.get(player.getName()));
            if (target == null || !target.isOnline()) {
                player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently offline!");
                return true;
            }

            String message = "";
            for (int i = 0; i != args.length; i++)
                message += args[i] + " ";
            PermissionUser sUser = PermissionsEx.getUser(player);
            PermissionUser targerUser = PermissionsEx.getUser(target);
            String sPrefix = sUser.getPrefix();
            String targetPrefix = targerUser.getPrefix();
            target.sendMessage(ChatColor.LIGHT_PURPLE + "From " + ChatColor.RESET + sPrefix + "" + player.getName() + " " + ChatColor.YELLOW + message);
            player.sendMessage(ChatColor.LIGHT_PURPLE + "To " + ChatColor.RESET + targetPrefix + "" + target.getName() + " " + ChatColor.YELLOW + message);
        }
        if (command.getName().equalsIgnoreCase("setmotd")) {
            if (!sender.hasPermission("surreal.motd.set")) {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
                return true;
            }
            if (args.length == 0) {
                sender.sendMessage(prefix + ChatColor.GOLD + "Please specify a message!");
                return true;
            }
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                str.append(args[i] + " ");
            }
            String motd = str.toString();
            getConfig().set("motd.system", motd);
            saveConfig();
            String system = getConfig().getString("motd.system");
            system = system.replaceAll("&", "�");
            sender.sendMessage(ChatColor.GREEN + "MOTD set to: " + system);
            return true;
        }
        if (command.getName().equalsIgnoreCase("mute")) {
            if (sender.hasPermission("surreal.mute")) {
                if (args.length == 0) {
                    player.sendMessage(prefix + ChatColor.GOLD + "/mute <player>");
                } else {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently offline!");
                        return true;
                    }
                    if (target.getName() == player.getName()) {
                        player.sendMessage(prefix + ChatColor.YELLOW + "You can not mute yourself!");
                        return true;
                    }
                    PermissionUser sUser = PermissionsEx.getUser(player);
                    PermissionUser targerUser = PermissionsEx.getUser(target);
                    if (sUser.inGroup("mod") && targerUser.inGroup("mod") || sUser.inGroup("mod") && targerUser.inGroup("admin") || sUser.inGroup("mod") && targerUser.inGroup("dev") || sUser.inGroup("admin") && targerUser.inGroup("dev")) {
                        player.sendMessage(prefix + ChatColor.GOLD + "You can not mute this person!");
                        return true;
                    } else {
                        String sPrefix = sUser.getPrefix();
                        String targetPrefix = targerUser.getPrefix();
                        if (mutedPlayers.contains(target.getName())) {
                            mutedPlayers.remove(target.getName());
                            targerUser.getPlayer().kickPlayer(ChatColor.YELLOW + "You have been unmuted by " + sPrefix + player.getName() + ChatColor.YELLOW + " happy chatting!");
                            target.sendMessage(prefix + ChatColor.GOLD + "You have been unmuted!");
                            openConnection();
                            try {
                                boolean muted;
                                if (playerDataContainsPlayer(player.getPlayer())) {
                                    PreparedStatement sql = connection.prepareStatement("SELECT muted FROM `surreal_data` WHERE player=?;");
                                    sql.setString(1, player.getPlayer().getName());

                                    ResultSet result = sql.executeQuery();
                                    result.next();

                                    muted = result.getBoolean("muted");

                                    PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET muted=? WHERE player=?");
                                    loginsupdate.setBoolean(1, false);
                                    loginsupdate.setString(2, player.getPlayer().getName());
                                    loginsupdate.executeUpdate();

                                    loginsupdate.close();
                                    sql.close();
                                    result.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                closeConnection();
                            }
                            for (Player staffy : staff) {
                                staffy.sendMessage(staffChatPrefix + sPrefix + player.getName() + ChatColor.GOLD + " has unmuted " + targetPrefix + "" + p.getName());
                            }
                        } else {
                            mutedPlayers.add(p.getName());
                            p.sendMessage(prefix + ChatColor.GOLD + "You have been Muted!");
                            for (Player staffy : staff) {
                                staffy.sendMessage(staffChatPrefix + sPrefix + player.getName() + ChatColor.GOLD + " has muted " + targetPrefix + "" + p.getName());
                            }
                            openConnection();
                            try {
                                boolean muted;
                                if (playerDataContainsPlayer(player.getPlayer())) {
                                    PreparedStatement sql = connection.prepareStatement("SELECT muted FROM `surreal_data` WHERE player=?;");
                                    sql.setString(1, player.getPlayer().getName());

                                    ResultSet result = sql.executeQuery();
                                    result.next();

                                    muted = result.getBoolean("muted");

                                    PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET muted=? WHERE player=?");
                                    loginsupdate.setBoolean(1, true);
                                    loginsupdate.setString(2, player.getPlayer().getName());
                                    loginsupdate.executeUpdate();

                                    loginsupdate.close();
                                    sql.close();
                                    result.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                closeConnection();
                            }
                        }
                    }
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("s")) {
            if (player.hasPermission("surreal.chat")) {
                if (args.length == 0) {
                    player.sendMessage(prefix + ChatColor.GOLD + "/s <message>");
                    return true;
                }
                PermissionUser sUser = PermissionsEx.getUser(player);
                String sPrefix = sUser.getPrefix();
                String staffmessage = "";
                for (int i = 0; i != args.length; i++)
                    staffmessage += args[i] + " ";
                for (Player staffy : staff) {
                    staffy.sendMessage(staffChatPrefix + sPrefix + player.getName() + ChatColor.GRAY + ": " + ChatColor.RESET + ChatColor.AQUA + staffmessage);
                }
            }
        }
        if (command.getName().equalsIgnoreCase("v")) {
            if (player.hasPermission("surreal.vanish")) {
                if (!(cooldown.contains(player.getName()))) {
                    cooldown.add(player.getName());
                    openConnection();
                    try {
                        boolean vanished;
                        PreparedStatement sql = connection.prepareStatement("SELECT vanished FROM `surreal_data` WHERE player=?;");
                        sql.setString(1, player.getName());

                        ResultSet result = sql.executeQuery();
                        result.next();
                        vanished = result.getBoolean("vanished");
                        PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET vanished=? WHERE player=?");
                        if (vanished == true) {
                            //Bukkit.getServer().getScheduler().cancelTask(tasks.get(player.getName()));
                            unvanish(player);
                            player.sendMessage(prefix + ChatColor.GOLD + "You have been unvanished!");
                            loginsupdate.setBoolean(1, false);
                            loginsupdate.setString(2, player.getName());
                            loginsupdate.executeUpdate();
                        }
                        if (vanished == false) {
                            vanish(player);
                            player.sendMessage(prefix + ChatColor.GOLD + "You are now vanished!");
                            loginsupdate.setBoolean(1, true);
                            loginsupdate.setString(2, player.getName());
                            loginsupdate.executeUpdate();

                            loginsupdate.close();
                            sql.close();
                            result.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        closeConnection();
                    }
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("SurrealCore"), new Runnable() {
                        @Override
                        public void run() {
                            cooldown.remove(player.getName());
                        }
                    }, 100L);
                } else {
                    player.sendMessage(prefix + ChatColor.GOLD + "Please wait 5 seconds to do this again!");
                }
            } else {
                player.sendMessage(prefix + ChatColor.RED + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("hacker")) {
            if (player.hasPermission("surreal.hacker")) {
                if (args.length == 0) {
                    player.sendMessage(prefix + ChatColor.GOLD + "/hacker <player>");
                    return true;
                }
                Player target = Bukkit.getPlayer(args[0]);
                if (target.getName() == player.getName()){
                    player.sendMessage(prefix + ChatColor.YELLOW + "You can not accuse yourself!");
                    return true;
                }
                if (target == null) {
                    player.sendMessage(prefix + ChatColor.YELLOW + "This player is currently offline!");
                    return true;
                } else {
                    target.sendMessage(prefix + ChatColor.DARK_AQUA + "We believe that you are hacking, this may be a mistake, if you do hack remove the hacked client now! Staff are now watching you.");
                    PermissionUser sUser = PermissionsEx.getUser(player);
                    String sPrefix = sUser.getPrefix();
                    PermissionUser targerUser = PermissionsEx.getUser(target);
                    String targetPrefix = targerUser.getPrefix();
                    for (Player staffy : staff) {
                        staffy.sendMessage(staffChatPrefix + sPrefix + player.getName() + ChatColor.GOLD + " has accused " + targetPrefix + "" + target.getName() + ChatColor.GOLD + " for hacking!");
                    }
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "You do not have permission to do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("home"))
        {
            if (!(sender instanceof Player))
            {
                sender.sendMessage("Console can not run this command!!");
            }
            else
            {
                if (args.length == 0) {
                    if ((getConfig().isSet("users." + player.getName() + ".home.world")) && (getConfig().isSet("users." + player.getName() + ".home.x")) && (getConfig().isSet("users." + player.getName() + ".home.y")) && (getConfig().isSet("users." + player.getName() + ".home.z")))
                    {
                        Location loc = new Location(getServer().getWorld(getConfig().getString("users." + player.getName() + ".home.world")), getConfig().getDouble("users." + player.getName() + ".home.x"), getConfig().getDouble("users." + player.getName() + ".home.y"), getConfig().getDouble("users." + player.getName() + ".home.z"));
                        loc.setYaw(getConfig().getInt("users." + player.getName() + ".home.d"));
                        player.teleport(loc);
                        player.sendMessage(prefix + ChatColor.GOLD + "Teleported you home!");
                    }
                    else
                    {
                        player.sendMessage(prefix + ChatColor.GOLD + "You do not have a home yet... The command is '/sethome'");
                    }
                }
                if (args.length == 1) {
                    if ((getConfig().isSet("users." + player.getName() + ".home." + args[0] + ".world")) && (getConfig().isSet("users." + player.getName() + ".home." + args[0] + ".x")) && (getConfig().isSet("users." + player.getName() + ".home." + args[0] + ".y")) && (getConfig().isSet("users." + player.getName() + ".home." + args[0] + ".z")))
                    {
                        Location loc = new Location(getServer().getWorld(getConfig().getString("users." + player.getName() + ".home." + args[0] + ".world")), getConfig().getDouble("users." + player.getName() + ".home." + args[0] + ".x"), getConfig().getDouble("users." + player.getName() + ".home." + args[0] + ".y"), getConfig().getDouble("users." + player.getName() + ".home." + args[0] + ".z"));
                        loc.setYaw(getConfig().getInt("users." + player.getName() + ".home." + args[0] + ".d"));
                        player.teleport(loc);
                        player.sendMessage(prefix + ChatColor.GOLD + "Teleported you home!");
                    }
                    else
                    {
                        player.sendMessage(prefix + ChatColor.GOLD + "You do not have a home yet... The command is '/sethome [Name]'");
                    }
                }
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("sethome"))
        {
            if (!(sender instanceof Player))
            {
                sender.sendMessage("Console can not run this command!!");
            }
            else
            {
                Location loc = player.getLocation();
                if (args.length == 0)
                {
                    getConfig().set("users." + player.getName() + ".home.world", loc.getWorld().getName());
                    getConfig().set("users." + player.getName() + ".home.x", Double.valueOf(loc.getX()));
                    getConfig().set("users." + player.getName() + ".home.y", Double.valueOf(loc.getY()));
                    getConfig().set("users." + player.getName() + ".home.z", Double.valueOf(loc.getZ()));
                    getConfig().set("users." + player.getName() + ".home.d", Float.valueOf(loc.getYaw()));
                    player.sendMessage(prefix + ChatColor.GOLD + "Your home was set!");
                    saveConfig();
                }
                else if (args.length == 1)
                {
                    getConfig().set("users." + player.getName() + ".home." + args[0] + ".world", loc.getWorld().getName());
                    getConfig().set("users." + player.getName() + ".home." + args[0] + ".x", Double.valueOf(loc.getX()));
                    getConfig().set("users." + player.getName() + ".home." + args[0] + ".y", Double.valueOf(loc.getY()));
                    getConfig().set("users." + player.getName() + ".home." + args[0] + ".z", Double.valueOf(loc.getZ()));
                    getConfig().set("users." + player.getName() + ".home." + args[0] + ".d", Float.valueOf(loc.getYaw()));
                    player.sendMessage(prefix + ChatColor.GOLD + "Your home was set!");
                    saveConfig();
                }
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("delhome"))
        {
            if (!(sender instanceof Player))
            {
                sender.sendMessage("Console can not run this command!!");
            }
            else
            {
                Location loc = player.getLocation();
                if (args.length == 0) {
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home.world", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home.x", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home.y", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home.z", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home.d", null);
                    player.sendMessage(prefix + ChatColor.GOLD + "Your home was deleted!");
                    saveConfig();
                } else if (args.length == 1)
                {
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home" + args[0] + ".world", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home" + args[0] + ".x", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home" + args[0] + ".y", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home" + args[0] + ".z", null);
                    getConfig().getConfigurationSection("users." + player.getName()).set(".home" + args[0] + ".d", null);
                    player.sendMessage(prefix + ChatColor.GOLD + "Your home '" +  args[0] + "' was deleted!");
                    saveConfig();
                }
            }
            return true;
        }
        if (command.getName().equalsIgnoreCase("spawn")){
            player.teleport(spawn);
        }
        return false;
    }

    @Override
    public void onDisable() {
        for (Player allServer : Bukkit.getOnlinePlayers()){
            allServer.kickPlayer(ChatColor.GOLD + "The server reload forced all players to be kicked! " + ChatColor.YELLOW + "");
        }
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://66.85.144.162:3306/mcph465133", "mcph465133", "6d797f967b");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static boolean playerDataContainsPlayer(Player player) {
        String uuid = player.getUniqueId().toString();
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM `surreal_data` WHERE uuid=?;");
            sql.setString(1, uuid);
            ResultSet resultSet = sql.executeQuery();
            boolean containsPlayer = resultSet.next();

            sql.close();
            resultSet.close();

            return containsPlayer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @EventHandler
    public void onServerPing(ServerListPingEvent e) {
        String motd = getConfig().getString("motd.system");
        e.setMotd(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "SurrealCraft " + ChatColor.DARK_GRAY + "- " + ChatColor.GOLD +  "BETA TESTING! \n" + ChatColor.YELLOW + motd);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PermissionUser sUser = PermissionsEx.getUser(event.getPlayer());
        String sPrefix = sUser.getPrefix();
        if (!player.hasPlayedBefore()){
            player.teleport(spawn);
        }
        event.setJoinMessage("" + sPrefix + "" + event.getPlayer().getName() + ChatColor.YELLOW + " has joined the server!");
        if (sUser.inGroup("user")) {
            sUser.addGroup("default");
            sUser.removeGroup("user");
        }
        if (sUser.inGroup("default") || (sUser.inGroup("VIP") || (sUser.inGroup("YT") || (sUser.inGroup("user"))))) {
            nonstaff.add(event.getPlayer());
        } else {
            staff.add(event.getPlayer());
        }
        event.getPlayer().getEyeLocation().setPitch(90);
        openConnection();
        try {
            boolean online;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT online FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getPlayer().getName());

                ResultSet result = sql.executeQuery();
                result.next();

                online = result.getBoolean("online");

                PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET online=? WHERE player=?");
                loginsupdate.setBoolean(1, true);
                loginsupdate.setString(2, event.getPlayer().getName());
                loginsupdate.executeUpdate();

                loginsupdate.close();
                sql.close();
                result.close();
            }
            boolean muted;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT muted FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getPlayer().getName());
                ResultSet result = sql.executeQuery();
                result.next();
                muted = result.getBoolean("muted");
                if (muted == true) {
                    mutedPlayers.add(event.getPlayer().getName());
                    event.getPlayer().sendMessage(prefix + ChatColor.GOLD + "You are still muted!");
                } else {
                    mutedPlayers.remove(event.getPlayer().getName());
                }
                sql.close();
                result.close();
            }
            boolean pvping;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT pvping FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getPlayer().getName());
                ResultSet result = sql.executeQuery();
                result.next();
                pvping = result.getBoolean("pvping");
                if (pvping == true) {
                    this.pvping.add(event.getPlayer().getName());
                } else {
                    this.pvping.remove(event.getPlayer().getName());
                }
                sql.close();
                result.close();
            }
            boolean flying;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT flying FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getPlayer().getName());
                ResultSet result = sql.executeQuery();
                result.next();
                flying = result.getBoolean("flying");
                if (flying == true) {
                    flyingStaff.add(event.getPlayer().getName());
                    event.getPlayer().sendMessage(prefix + ChatColor.GOLD + "You are still flying!");
                    event.getPlayer().setAllowFlight(true);
                    event.getPlayer().setFlying(true);
                }
                sql.close();
                result.close();
            }

            boolean vanished;
            PreparedStatement sql = connection.prepareStatement("SELECT vanished from `surreal_data` WHERE player=?;");
            sql.setString(1, event.getPlayer().getName());

            ResultSet result = sql.executeQuery();
            result.next();

            vanished = result.getBoolean("vanished");

            if (vanished == true) {
                Vanished.add(player.getName());
                vanish(event.getPlayer());
                event.getPlayer().sendMessage(prefix + ChatColor.GOLD + "You are still in vanish!");
            }
            sql.close();
            result.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        openConnection();
        try {
            int previousDeaths = 0;
            if (playerDataContainsPlayer(event.getEntity().getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT deaths FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getEntity().getPlayer().getName());

                ResultSet result = sql.executeQuery();
                result.next();

                previousDeaths = result.getInt("deaths");

                PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET deaths=? WHERE player=?");
                loginsupdate.setInt(1, previousDeaths + 1);
                loginsupdate.setString(2, event.getEntity().getPlayer().getName());
                loginsupdate.executeUpdate();

                loginsupdate.close();
                sql.close();
                result.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent event)
    {
        if (((event.getDamager() instanceof Player)) && ((event.getEntity() instanceof Player)))
        {
            Player damaged = (Player)event.getEntity();
            Player damager = (Player)event.getDamager();
            if ((this.pvping.contains(damager.getName())) || (this.pvping.contains(damaged.getName()))) {
                event.setCancelled(true);
            }
        }
        else if (((event.getDamager() instanceof Projectile)) && ((event.getEntity() instanceof Player)))
        {
            Projectile p = (Projectile)event.getDamager();
            if ((p.getShooter() instanceof Player))
            {
                Player damager = (Player)event.getDamager();
                if (pvping.contains(damager.getName())) {
                    event.setCancelled(true);
                }
            }
        }
    }
    @EventHandler
    public void leave(PlayerQuitEvent event){
        openConnection();
        try {
            boolean online;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sql = connection.prepareStatement("SELECT online FROM `surreal_data` WHERE player=?;");
                sql.setString(1, event.getPlayer().getName());

                ResultSet result = sql.executeQuery();
                result.next();

                online = result.getBoolean("online");

                PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `surreal_data` SET online=? WHERE player=?");
                loginsupdate.setBoolean(1, false);
                loginsupdate.setString(2, event.getPlayer().getName());
                loginsupdate.executeUpdate();

                loginsupdate.close();
                sql.close();
                result.close();
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    @EventHandler
    public void login(PlayerLoginEvent event) {
        String uuid = event.getPlayer().getUniqueId().toString();
        openConnection();
        try {
            int previousLogins = 0;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement statement = connection.prepareStatement("SELECT logins FROM `surreal_data` WHERE player=?;");
                statement.setString(1, event.getPlayer().getName());
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();

                previousLogins = resultSet.getInt("logins");

                PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `surreal_data` SET logins=? WHERE player=?");
                loginsUpdate.setInt(1, previousLogins + 1);
                loginsUpdate.setString(2, event.getPlayer().getName());
                loginsUpdate.executeUpdate();

                loginsUpdate.close();
                statement.close();
                resultSet.close();
            } else {
                PreparedStatement newPlayer = connection.prepareStatement("INSERT INTO `surreal_data`(`uuid`, `player`, `logins`, `kills`, `deaths`, `muted`, `online`, `coins`, `claimchunks`, `vanished`, `flying`, `pvping`) VALUES (uuid,?,1,0,0, FALSE, TRUE, 200, 1, FALSE, FALSE, FALSE);");
                newPlayer.setString(1, event.getPlayer().getName());
                newPlayer.execute();
                newPlayer.close();
                PreparedStatement statement = connection.prepareStatement("SELECT uuid FROM `surreal_data` WHERE player=?;");
                statement.setString(1, event.getPlayer().getName());
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();
                PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `surreal_data` SET uuid=? WHERE player=?");
                loginsUpdate.setString(1, uuid);
                loginsUpdate.setString(2, event.getPlayer().getName());
                loginsUpdate.executeUpdate();

                loginsUpdate.close();
                statement.close();
                resultSet.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    @EventHandler
    public void playerChat(AsyncPlayerChatEvent event) {
        if (mutedPlayers.contains(event.getPlayer().getName())) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(prefix + ChatColor.GOLD + "You are still muted!");
        } else {
            Player player = (Player) event.getPlayer();
            PermissionUser sUser = PermissionsEx.getUser(player);
            String sPrefix = sUser.getPrefix();
            if (sUser.inGroup("default")) {
                event.setFormat(sPrefix + "" + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + event.getMessage());
            } else {
                event.setFormat(sPrefix + "" + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + event.getMessage());
            }
        }
    }
}
